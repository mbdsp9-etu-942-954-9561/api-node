import mongoose from 'mongoose'


async function connectWithMongoose(databaseUri, databaseName) {

    try {

        mongoose.set('strictQuery', false)

        await mongoose.connect(databaseUri, {
            dbName: databaseName,
        })

        console.log('MongoDB Connected...')

    } catch (error) {

        console.log(`MongoDB Connection failed due to:\n${error}\n`)
        process.exit(1)

    }

}


export { connectWithMongoose }