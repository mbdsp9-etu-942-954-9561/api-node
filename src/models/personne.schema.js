import mongoose from 'mongoose'
import paginate from 'mongoose-paginate-v2'
import {baseModel} from '../utils/utils.js'

const PersonneSchema = new mongoose.Schema(
    baseModel({
        _id: {
            type: String,
            required: true,
            unique: true
        },
        citizenid: {
            type: Number,
            required: true,
            unique: true
        },
        name: String,
        firstname: String,
        birthday: Date,
        gender: String,
        idcard: {
            type: String,
            required: false,
        },
        deathdate: String,
        lifeStatus: {
            id: Number,
            label: String
        },
        password: String,
    }),
    {
        timestamps: true,
        _id: false
    }
)


PersonneSchema.plugin(paginate)

export const Personne = mongoose.model('Personne', PersonneSchema, 'personne')
