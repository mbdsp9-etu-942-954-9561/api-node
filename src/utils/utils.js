import * as CryptoJS from 'crypto-js'

import html_to_pdf from 'html-pdf-node'
import createError from 'http-errors'
import jwt from 'jsonwebtoken'
import mongoose from 'mongoose'
import * as nodemailer from 'nodemailer'
import validator from 'validator'
import {Settings} from '../config/settings.js'
import {CONSTANTS} from './constants.js'


export function isEmpty(value) {

    return (
        value === null || // check for null
        value === undefined || // check for undefined
        value === '' || // check for empty string
        (Array.isArray(value) && value.length === 0) || // check for empty array
        (typeof value === 'object' && Object.keys(value).length === 0) // check for empty object
    )

}


export function baseModel(schemaDefinition) {

    return Object.assign(schemaDefinition, {
        deleted: {
            type: Boolean,
            required: true,
            default: false
        },
    })

}

export function customLabels() {

    return CONSTANTS.custom_labels

}


export function toDocumentFormat(userData) {

    return Object.assign(userData, {_id: String(new mongoose.mongo.ObjectId())})

}


export function toResponseEntity(status, message = '', data = undefined) {

    const response = {message: isEmpty(message) ? 'Data found' : message, status: status}

    if (!isEmpty(data)) Object.assign(response, {data: data})

    return response

}


