export const CONSTANTS = {
    'custom_labels': {
        'totalDocs': 'totalItems',
        'docs': 'items',
        'limit': 'itemsPerPage',
        'meta': 'paginator'
    },
    'password_options': {
        'length': 8,
        'numbers': true,
        'symbols': true,
        'strict': true,
        'exclude': '~{[|`\\^]}¤²<>'
    }
}

