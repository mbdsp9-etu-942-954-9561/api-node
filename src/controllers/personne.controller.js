import {Router} from 'express'
import {PersonneDto, PersonneVS} from '../dto/personne.dto.js'
import {validationMiddleware} from '../middlewares/validation.middleware.js'
import {PersonneService} from "../services/personne.service.js";
import {isEmpty} from '../utils/utils.js'

const router = Router()

const personneService = new PersonneService()

router.post('', validationMiddleware(PersonneVS, PersonneDto), async (request, response) => {

    const userData = request.body
    const user = await personneService.create(userData)
    response.status(200).json(user)

})

router.get('', async (request, response) => {


    const optionsData = isEmpty(request.query.options) ? {pagination: false} : JSON.parse(request.query.options)
    const queryData = isEmpty(request.query.query) ? {} : JSON.parse(request.query.query)
    const personne = await personneService.find(queryData, optionsData)

    response.status(200).json(personne)


})

router.put('/death/:personne_id', validationMiddleware(PersonneVS, PersonneDto), async (request, response) => {

    try {

        const personneData = request.body
        const personneId = request.params.personne_id
        const personne = await personneService.updateDeath(personneId, personneData)

        response.status(200).json( personne)

    } catch (error) {

        response.status(200).json(error)

    }

})

export {router as PersonneRouter}