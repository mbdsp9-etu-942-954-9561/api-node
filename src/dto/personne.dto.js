import yup from 'yup'

const LifeStatusVS = yup.object().shape({
    id: yup.number(),
    label: yup.string(),
})

export const PersonneVS = yup.object().shape({
    citizenid: yup.number(),
    name: yup.string().min(1, 'Name must be more than 2 characters').required(),
    firstname: yup.string().min(1, 'Name must be more than 2 characters').required(),
    birthday: yup.date(),
    gender: yup.string(),
    idcard: yup.string().nullable(),
    deathdate: yup.date().nullable(),
    lifeStatus: LifeStatusVS,
    password: yup.string().min(1, 'Name must be more than 2 characters')
})

export class PersonneDto {

    citizenid;
    name;
    firstname;
    birthday;
    gender;
    idcard;
    deathdate;
    lifeStatus;
    password;


    constructor({citizenid, name, firstname, birthday, gender, idcard, deathdate, lifeStatus, password}) {

        this.citizenid = citizenid
        this.name = name
        this.firstname = firstname
        this.birthday = birthday
        this.gender = gender
        this.idcard = idcard
        this.deathdate = deathdate
        this.lifeStatus = lifeStatus
        this.password = password

    }

}
