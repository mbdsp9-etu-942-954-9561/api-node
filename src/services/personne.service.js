import createError from 'http-errors'
import {customLabels, isEmpty, toDocumentFormat} from '../utils/utils.js'
import {Personne} from "../models/personne.schema.js";


export class PersonneService {

    constructor() {

    }


    async find(query, options) {

        query = Object.assign(isEmpty(query) ? {} : query, { deleted: false })

        options = Object.assign(isEmpty(options) ? {} : options, {
            lean: true,
            allowDiskUse: true,
            customLabels: customLabels()
        })

        return await Personne.paginate(query, options)

    }


    async findById(citoyenId) {

        if (isEmpty(citoyenId)) throw createError(409, 'No citoyen ID trouvé')

        return Personne
            .findOne({ _id: citoyenId, deleted: false })
            .lean()

    }

    async create(userData) {

        const personne = new Personne(toDocumentFormat(userData))

        await personne.save()

        return await this.findById(personne._id)

    }

    async updateDeath(personneId, personneData) {

        if (isEmpty(personneId)) throw createError(409, 'No personne ID found')

        const personneCurrent = await Personne.findById(personneId)

        personneCurrent.deathdate = personneData.deathdate

        await personneCurrent.save()

        return await this.findById(personneId)

    }

}
